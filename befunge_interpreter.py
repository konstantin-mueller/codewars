import random
from typing import Tuple


class Interpreter:
    def __init__(self, code: str) -> None:
        self.code = code.split("\n")
        self.stack = []

        self._output = ""
        self._move_dir = ">"
        self._string_mode = False
        self._instructions = {
            "+-*/%": self._calc,
            "!": self._logical_not,
            "`": self._greater_than,
            ".": self._add_to_output,
            "_": self._move_right_or_left_based_on_pop,
            "|": self._move_down_or_up_based_on_pop,
            '"': self._toggle_string_mode,
            ":": self._duplicate,
            "\\": self._swap,
            "$": self._discard,
            ",": self._add_ascii_to_output,
            " ": self._no_op,
            "p": self._put,
            "g": self._get,
        }

    def interpret(self) -> str:
        i = 0
        j = 0
        char = self.code[i][j]
        while char != "@":
            if char == "?":
                char: str = random.choice("<>^v")
            if char in "<>^v":
                i, j = self._move(char, i, j)
                self._move_dir = char
                char = self.code[i][j]
                continue

            if self._string_mode and char != '"':
                self._exec_string_mode(char)
            elif char == "#":
                i, j = self._move(self._move_dir, i, j)
            else:
                self._interpret(char)

            i, j = self._move(self._move_dir, i, j)
            char = self.code[i][j]
        return self._output

    def _interpret(self, char: str) -> None:
        if char.isdigit():
            self.stack.append(int(char))
            return

        next(
            function
            for instruction, function in self._instructions.items()
            if char in instruction
        )(char)

    def _pop(self) -> Tuple[int, int]:
        return self.stack.pop(), self.stack.pop()

    def _move(self, move_dir: str, i: int, j: int) -> Tuple[int, int]:
        if move_dir == ">":
            j += 1
        elif move_dir == "<":
            j -= 1
        elif move_dir == "^":
            i -= 1
        elif move_dir == "v":
            i += 1

        if i < 0:
            i = len(self.code) - 1
        elif i >= len(self.code):
            i = 0

        if j < 0:
            i -= 1
            if i < 0:
                i = len(self.code) - 1

            j = len(self.code[i]) - 1
        elif j >= len(self.code[i]):
            i += 1
            if i >= len(self.code):
                i = 0

            j = 0

        return i, j

    def _calc(self, char: str) -> None:
        if char in "+*":
            a, b = self._pop()
        else:
            b, a = self._pop()

        if char in "/%" and a == 0:
            self.stack.append(0)
            return

        if char == "/":
            char = "//"

        self.stack.append(eval(f"{a}{char}{b}"))

    def _logical_not(self, char: str) -> None:
        if self.stack.pop() == 0:
            self.stack.append(1)
        else:
            self.stack.append(0)

    def _greater_than(self, char: str) -> None:
        a, b = self._pop()
        if b > a:
            self.stack.append(1)
        else:
            self.stack.append(0)

    def _add_to_output(self, char: str) -> None:
        self._output = f"{self._output}{self.stack.pop()}"

    def _move_right_or_left_based_on_pop(self, char: str) -> None:
        self._move_dir = ">" if self.stack.pop() == 0 else "<"

    def _move_down_or_up_based_on_pop(self, char: str) -> None:
        self._move_dir = "v" if self.stack.pop() == 0 else "^"

    def _toggle_string_mode(self, char: str) -> None:
        self._string_mode = not self._string_mode

    def _exec_string_mode(self, char: str) -> None:
        if not self._string_mode:
            return

        self.stack.append(ord(char))

    def _duplicate(self, char: str) -> None:
        self.stack.append(0 if len(self.stack) == 0 else self.stack[-1])

    def _swap(self, char: str) -> None:
        first = self.stack.pop()
        second = 0 if len(self.stack) == 0 else self.stack.pop()
        self.stack.append(first)
        self.stack.append(second)

    def _discard(self, char: str) -> None:
        del self.stack[-1]

    def _add_ascii_to_output(self, char: str) -> None:
        self._output += chr(self.stack.pop())

    def _put(self, char: str) -> None:
        x, y = self._pop()
        v = self.stack.pop()
        self.code[x] = self.code[x][:y] + chr(v) + self.code[x][y + 1 :]

    def _get(self, char: str) -> None:
        x, y = self._pop()
        self.stack.append(ord(self.code[x][y]))

    def _no_op(self, char: str) -> None:
        pass


def interpret(code: str) -> str:
    return Interpreter(code).interpret()
