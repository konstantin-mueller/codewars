import pytest

from befunge_interpreter import Interpreter


@pytest.fixture
def interpreted(chars: str) -> str:
    interp = Interpreter(chars)
    return interp.interpret()


@pytest.mark.parametrize(
    "chars", ["0.@", "1.@", "2.@", "3.@", "4.@", "5.@", "6.@", "7.@", "8.@", "9.@"]
)
def test_interpreter_should_push_number_onto_interpreted(interpreted, chars):
    assert interpreted == chars[0]


@pytest.mark.parametrize(
    "chars, expected",
    [
        ("12+.@", "3"),
        ("21-.@", "1"),
        ("12*.@", "2"),
        ("42/.@", "2"),
        ("32/.@", "1"),
        ("01/.@", "0"),
        ("95%.@", "4"),
        ("05%.@", "0"),
    ],
)
def test_interpreter_should_calc_result(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize(
    "chars, expected", [("0!.@", "1"), ("1!.@", "0"), ("2!.@", "0")]
)
def test_interpreter_should_push_logical_not_value(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize(
    "chars, expected", [("00`.@", "0"), ("12`.@", "0"), ("21`.@", "1")]
)
def test_interpreter_should_push_greater_than_value(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize(
    "chars, expected",
    [
        ("v>1.@\n>^", "1"),
        (">12+v\nv-12<\n>>35*++.@", "19"),
        ("<@.2", "2"),
        ("?2.@.2\n2", "2"),
    ],
)
def test_interpreter_should_move(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize(
    "chars, expected",
    [("20_.@", "2"), ("2_@.", "2"), ("20|\n@.<", "2"), ("2|\n @\n .\n 2", "2")],
)
def test_interpreter_should_move_based_on_value_on_stack(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize("chars, expected", [('"a".@', "97"), ('"abc"...@', "999897")])
def test_interpreter_should_push_ascii_values(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize("chars, expected", [(":.@", "0"), ("2:..@", "22")])
def test_interpreter_should_duplicate_value_on_stack(interpreted, expected):
    assert interpreted == expected


@pytest.mark.parametrize("chars, expected", [("2\\..@", "02"), ("23\\..@", "23")])
def test_interpreter_should_swap_values_on_stack(interpreted, expected):
    assert interpreted == expected


def test_interpreter_should_discard():
    interpreter = Interpreter("22$.@")

    assert interpreter.interpret() == "2"
    assert len(interpreter.stack) == 0


@pytest.mark.parametrize("chars, expected", [('"a",@', "a"), ('"abc",,,@', "cba")])
def test_interpreter_should_output_ascii_values(interpreted, expected):
    assert interpreted == expected


def test_interpreter_should_skip_cell_with_trampoline():
    interpreter = Interpreter("#2#.2.@")

    assert interpreter.interpret() == "2"
    assert len(interpreter.stack) == 0


def test_interpreter_should_put_value_in_program():
    interpreter = Interpreter('"a"70p"b",@')

    assert interpreter.interpret() == "a"
    assert interpreter.code == ['"a"70p"a",@']


def test_interpreter_should_get_value_in_program():
    interpreter = Interpreter('50g,"a"@')

    assert interpreter.interpret() == "a"
    assert interpreter.stack == [97]
